﻿using System;
using Util;

namespace CubeSummation
{
    public partial class Default : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void BtnEjecutar_Click(object sender, EventArgs e)
        {
            ProcesarCubo procesar = new ProcesarCubo();
            procesar.setEntrada(TxtEntrada.Text);
            TxtSalida.Text = procesar.Ejecutar();
        }

        protected void BtnPorDefecto_Click(object sender, EventArgs e)
        {
            TxtEntrada.Text = "2\n" +
                            "4 5\n" +
                            "UPDATE 2 2 2 4\n" +
                            "QUERY 1 1 1 3 3 3\n" +
                            "UPDATE 1 1 1 23\n" +
                            "QUERY 2 2 2 4 4 4\n" +
                            "QUERY 1 1 1 3 3 3\n" +
                            "2 4\n" +
                            "UPDATE 2 2 2 1\n" +
                            "QUERY 1 1 1 1 1 1\n" +
                            "QUERY 1 1 1 2 2 2\n" +
                            "QUERY 2 2 2 2 2 2";

        }
    }
}