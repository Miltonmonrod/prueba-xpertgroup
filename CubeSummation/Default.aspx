﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CubeSummation.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="BtnPorDefecto" runat="server" Text="Cargar datos de prueba por defecto" OnClick="BtnPorDefecto_Click" />
            <br />
            <asp:TextBox ID="TxtEntrada" runat="server" Height="230px" TextMode="MultiLine" Width="590px"></asp:TextBox>
            <br />
            <asp:Button ID="BtnEjecutar" runat="server" Text="Ejecutar" OnClick="BtnEjecutar_Click" />
            <br />
            <asp:TextBox ID="TxtSalida" runat="server" Height="230px" TextMode="MultiLine" Width="590px"></asp:TextBox>
        </div>
    </form>
</body>
</html>
