﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Util
{

    public class ProcesarCubo
    {

        private String entrada;

        public ProcesarCubo()
        {
        }

        public string Ejecutar()
        {
            StringBuilder salida = new StringBuilder();
            try
            {                
                LeerCubo leer = new LeerCubo(this.entrada);

                //T = número de casos de pruebas 
                //n = Define la matriz 
                //m = numero de operaciones (Actualizaciones o consultas)
                int T = leer.siguienteInt(), i = 0, n = 0, m = 0;

                //Ejecutamos un ciclo para operar el número de casos
                for (i = 0; i < T; i++)
                {
                    //Estructura de datos para almacenar las operaciones de UPDATE y posteriormente utilizarla para los QUERY
                    Dictionary<String, long> dict = new Dictionary<String, long>();
                    n = leer.siguienteInt();
                    m = leer.siguienteInt();
                    for (int q = 0; q < m; q++)
                    {
                        String operacion = leer.siguientePalabra().ToUpper();
                        if (operacion.Equals(Constantes.UPDATE))
                        {
                            //Coordenadas
                            int x, y, z;
                            //Actualizar valor
                            long w;
                            //Obtener valores
                            x = leer.siguienteInt();
                            y = leer.siguienteInt();
                            z = leer.siguienteInt();
                            w = leer.siguienteLong();
                            //Almacenar coordenadas
                            dict.Add(String.Format("{0}{1}{2}{3}{4}", x, Constantes.SEPARADOR, y, Constantes.SEPARADOR, z), w);
                        }
                        else if (operacion.Equals(Constantes.QUERY))
                        {
                            //Delimitar coordenadas
                            int x1, y1, z1, x2, y2, z2;
                            //Variable para sumar los valores
                            long resultado = 0;
                            //Obtener valores
                            x1 = leer.siguienteInt();
                            y1 = leer.siguienteInt();
                            z1 = leer.siguienteInt();
                            x2 = leer.siguienteInt();
                            y2 = leer.siguienteInt();
                            z2 = leer.siguienteInt();
                            //ITerar sobre el diccionario de valores
                            foreach (var item in dict.AsEnumerable())
                            {
                                //coordenadas
                                int x, y, z;
                                //Convertir la llave a coordenadas
                                String[] key = item.Key.Split(Constantes.SEPARADOR);
                                x = Convert.ToInt32(key[0]);
                                y = Convert.ToInt32(key[1]);
                                z = Convert.ToInt32(key[2]);
                                //Validar las coordenadas
                                if ((x >= x1 && x <= x2) && (y >= y1 && y <= y2) && (z >= z1 && z <= z2))
                                    //Sumar los valores 
                                    resultado += item.Value;
                            }
                            //almacenar los resultados
                            salida.Append(string.Format("{0}\n", resultado.ToString()));
                        }
                        else
                        {
                            salida.Append(string.Format("{0}\n", "Operación inexistente"));
                            break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                salida.Append(ex != null ? ex.Message : "Error en la estructura de la entrada de la prueba.");
            }
            return salida.ToString().Trim();
        }
    
        public String getEntrada() {
            return this.entrada;
        }

        public void setEntrada(string entrada)
        {
            this.entrada = entrada;
        }

    }
}