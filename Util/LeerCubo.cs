﻿using System.IO;
using System.Text;

namespace Util
{
    public class LeerCubo : StringReader
    {
        string palabraActual;

        public LeerCubo(string source) : base(source)
        {
            leerSiguientePalabra();
        }

        private void leerSiguientePalabra()
        {
            StringBuilder sb = new StringBuilder();
            char siguienteCaracter;
            int siguiente;
            do
            {
                siguiente = this.Read();
                if (siguiente < 0)
                    break;
                siguienteCaracter = (char)siguiente;
                if (char.IsWhiteSpace(siguienteCaracter))
                    break;
                sb.Append(siguienteCaracter);
            } while (true);
            while ((this.Peek() >= 0) && (char.IsWhiteSpace((char)this.Peek())))
                this.Read();
            if (sb.Length > 0)
                palabraActual = sb.ToString();
            else
                palabraActual = null;
        }

        public bool tieneSiguienteInt()
        {
            if (palabraActual == null)
                return false;
            int dummy;
            return int.TryParse(palabraActual, out dummy);
        }

        public int siguienteInt()
        {
            try
            {
                return int.Parse(palabraActual);
            }
            finally
            {
                leerSiguientePalabra();
            }
        }

        public bool tieneSiguienteLong()
        {
            if (palabraActual == null)
                return false;
            long dummy;
            return long.TryParse(palabraActual, out dummy);
        }

        public long siguienteLong()
        {
            try
            {
                return long.Parse(palabraActual);
            }
            finally
            {
                leerSiguientePalabra();
            }
        }

        public bool tieneSiguiente()
        {
            return palabraActual != null;
        }

        public bool tieneSiguientePalabra()
        {
            return !string.IsNullOrEmpty(palabraActual);
        }

        public string siguientePalabra()
        {
            try
            {
                return palabraActual;
            }
            finally
            {
                leerSiguientePalabra();
            }
        }
       
    }
}
