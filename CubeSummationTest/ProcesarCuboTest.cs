﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Util;

namespace CubeSummationTest
{
    [TestClass]
    public class ProcesarCuboTest
    {
        [TestMethod]
        public void EjecutarTest()
        {
            //Entrada de la prueba
            string Entrada = "2\n" +
                            "4 5\n" +
                            "UPDATE 2 2 2 4\n" +
                            "QUERY 1 1 1 3 3 3\n" +
                            "UPDATE 1 1 1 23\n" +
                            "QUERY 2 2 2 4 4 4\n" +
                            "QUERY 1 1 1 3 3 3\n" +
                            "2 4\n" +
                            "UPDATE 2 2 2 1\n" +
                            "QUERY 1 1 1 1 1 1\n" +
                            "QUERY 1 1 1 2 2 2\n" +
                            "QUERY 2 2 2 2 2 2";
            //Resultado esperado
            var resultadoEsperado = "4\n" +
                                    "4\n" +
                                    "27\n" +
                                    "0\n" +
                                    "1\n" +
                                    "1";
            //Ejecutar prueba
            ProcesarCubo procesar = new ProcesarCubo();
            procesar.setEntrada(Entrada);
            //Comparar resultados
            Assert.AreEqual(resultadoEsperado, procesar.Ejecutar());
       }
    }
}
